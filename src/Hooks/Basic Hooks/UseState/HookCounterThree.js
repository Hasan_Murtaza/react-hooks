import React, { useState } from 'react'

function HookCounterThree() {
    
    const [user, setUser] = useState({first_name: "", last_name: ""})

    function handleChange(event) {
        const { name, value } = event.target;
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }));
    }
    return (
        <div>
            <form action="">
                <input type="text" name="first_name" onChange={handleChange} value={user.first_name} 
                
                 />
                <input type="text" name="last_name" onChange={handleChange} value={user.last_name} 
              
                />
                <h2>Your First Name is - { user.first_name }</h2>
                <h2>Your last Name is - { user.last_name }</h2>
                <p>{JSON.stringify(user)}</p>
            </form>
        </div>
    )
}

export default HookCounterThree
