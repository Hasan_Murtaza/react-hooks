import React, { useState } from 'react'

function HookCounterTwo() {

    // Declare Initialstate value pass in useState hook
    const initialState = 0;

    //Declare State 
    const [count, setCount] = useState(initialState)

    // here we are declare function 
    function reSet() {
        setCount( initialState)
    }

    /**
     * What is  prevCount

    * Just like when we need to update state using setState, 
    * we either passed an object or a function to setState. 
    * The function takes prevState and prevProps as parameters. 
    * Since setState is async, it is always advisable to update state passing a function rather an object.
    * Likewise setCount takes a function. 
    * Remember useState returns 1. the initial state value 2. a function to update the state
     */

    function IncrementFive() {
        for (let index = 0; index < 5; index++) {
            setCount(prevCount => prevCount + 1)
            
        }
    }
    return (
        <div>
            Count: { count } <br/>
            <button onClick={reSet}>Reset</button><br />
            <button onClick={() => setCount(count + 1)}>Increment</button><br />
            <button onClick={() => setCount(count - 1)}>Decrement</button><br />
            <button onClick={IncrementFive}>Increment five</button>
        </div>
    )
}

export default HookCounterTwo
