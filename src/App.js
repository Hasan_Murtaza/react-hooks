
// UseState
// import ClassCounter from './Hooks/Basic Hooks/UseState/ClassCounter';
// import HookCounterOne from "./Hooks/Basic Hooks/UseState/HookCounterOne"
// import HookCounterTwo from "./Hooks/Basic Hooks/UseState/HookCounterTwo"
// import HookCounterThree from "./Hooks/Basic Hooks/UseState/HookCounterThree"
// import HookCounterFour from "./Hooks/Basic Hooks/UseState/HookCounterFour"

//UseEFfect
// import ClassCounter from "./Hooks/Basic Hooks/UseEffect/ClassCounter"
import HooksCounterOne from "./Hooks/Basic Hooks/UseEffect/HooksCounterOne"
function App() {
  return (
    <div className="App">
     {/* <ClassCounter /> */}
     {/* <HookCounterOne /> */}
     {/* <HookCounterTwo /> */}
     {/* <HookCounterThree /> */}
     {/* <HookCounterFour /> */}

    {/* <ClassCounter /> */}
    <HooksCounterOne />
    </div>
  );
}

export default App;
