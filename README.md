# Hooks

### Summary useState
1. The useState hooks lets you add state to functional components.
2. In classes, the state is always an object.
3. With the useState hook, the state doesn't have to be an object.
4. The useState hook return an array with two elements.
5. The first element is the current value of the state, and the second element is a state setter function.
6. New state value depends on the previous state value? you can pass a function to the setter function.
7. When dealing with the object or array always make sure to spread you state variable and then call the setter function.

